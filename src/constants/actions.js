// control panel actions
export const SWITCH_ITEM = 'SWITCH_ITEM';
export const SWITCH_MEASURE_MODE = 'SWITCH_MEASURE_MODE';
export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';