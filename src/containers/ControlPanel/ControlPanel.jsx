import iScroll from 'iscroll';
import React, { Component } from 'react';
import ReactIScroll from 'react-iscroll';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ItemBox } from '../../components/ControlPanel/ItemBox';
import * as actionCreators from './actions';
import './controlPanel.css';

export class ControlPanel extends Component {

    itemHoveredHandler = (item, mode) => {
        if (!this.props.currentItems.filter(i => i.id === item.id).length) {
            this.props.switchItem(item, mode);
        }
    }

    switchMeasureMode = (newMode) => {
        if(this.props.measureMode === newMode) return;

        this.props.switchMeasureMode(newMode);
        localStorage.setItem('measureMode', newMode);
    }

    isItemActive(item) {
        return this.props.currentItems.find(i => i.id === item.id);
    }

    render() {
        const itemWidth = 15;
        const computedWidth = itemWidth * Object.keys(this.props.modes).reduce((sum, mode) => {
            return sum + this.props.modes[mode].items.length;
        }, 0) + 'em';
        const options = {
            mouseWheel: true,
            scrollbars: true,
            scrollX: true,
            scrollY: false,
            click: true
        };
        const { measureMode } = this.props;

        return (
            <div>
                <div className="switchers-group">
                    <button className="inButton" data-active={measureMode === 'in'} onClick={() => this.switchMeasureMode('in')}>IN</button>
                    <button className="cmButton" data-active={measureMode === 'cm'} onClick={() => this.switchMeasureMode('cm')}>CM</button>
                </div>
                <div className="control-panel">
                    <ReactIScroll iScroll={iScroll} options={options}>
                        <div className="iscroll-wrap" style={{ width: computedWidth }}>
                            <ul>
                                {
                                    Object.keys(this.props.modes).map(mode =>
                                        this.props.modes[mode].items.map(item => 
                                            <ItemBox 
                                                item={item} 
                                                isActive={this.isItemActive(item)} 
                                                onHovered={() => this.itemHoveredHandler(item, mode)} 
                                                measureMode={measureMode} />
                                        )
                                    )
                                }
                            </ul>
                        </div>
                    </ReactIScroll>
                </div>
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        modes: state.ControlPanel.modes,
        currentItems: state.ControlPanel.currentItems,
        measureMode: state.ControlPanel.currentMeasureMode
    };
};

let mapDispatchToProps = (dispatch) => bindActionCreators(actionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ControlPanel);