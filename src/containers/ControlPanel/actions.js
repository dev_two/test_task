import { ADD_ITEM, REMOVE_ITEM, SWITCH_ITEM, SWITCH_MEASURE_MODE } from '../../constants/actions';

export function switchItem(item, mode) {
    return {
        type: SWITCH_ITEM,
        payload: { item, mode }
    };
}

export function switchMeasureMode(mode) {
    return {
        type: SWITCH_MEASURE_MODE,
        payload: { mode }
    };
}

export function addItem(item) {
    return {
        type: ADD_ITEM,
        payload: item
    };
}

export function removeItem(item) {
    return {
        type: REMOVE_ITEM,
        payload: item
    };
}