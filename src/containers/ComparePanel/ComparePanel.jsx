import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CompareWithItem } from '../../components/ComparePanel/CompareWithItem';
import { Header } from '../../components/Header/Header';
import { calculations } from '../../core/ui/calculations';
import './comparePanel.css';

export class ComparePanel extends Component {
    constructor(props) {
        super(props);
        this.state = { domReady: false, key: 0 };
    }

    componentDidMount() {
        this.setState({ domReady: true });

        window.onorientationchange = this.updateKey
        window.onresize = this.updateKey
    }

    updateKey = () => this.setState({ key: Math.random() });

    showCurrentMode(mode, product, item) {
        const
            width = this.comparePanelBody.offsetWidth,
            height = this.comparePanelBody.offsetHeight,
            measureMode = this.props.measureMode;

        return (
            <CompareWithItem 
                key={item.id} 
                product={product}
                item={item} 
                calculations={ calculations(width, height) } 
                bodySize = {{width, height}}
                measureMode={measureMode}
            />
        )
    }

    setRef = (node) => {
        if(node === null) return;

        this.setState({ domReady: false });

        this.comparePanelBody = node;

        // Just after ref update is returns the same value, but after setImmediate fires it is already updated 🤷‍♂️
        setImmediate(() => this.setState({ domReady: true }));
    }

    render() {
        return (
            <div className="compare-panel">
                <Header mode={this.props.currentMode} productName={this.props.product.name} currentItems={this.props.currentItems} />
                <div className="compare-panel__body" ref={this.setRef} key={this.state.key}>
                    {
                        this.state.domReady &&
                        this.showCurrentMode(this.props.currentMode, this.props.product, this.props.currentItems[0])
                    }
                </div>
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        currentMode: state.ControlPanel.currentMode,
        currentItems: state.ControlPanel.currentItems,
        product: state.ComparePanel.product,
        measureMode: state.ControlPanel.currentMeasureMode
    };
};


export default connect(mapStateToProps)(ComparePanel);